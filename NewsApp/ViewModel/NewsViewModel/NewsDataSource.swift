//
//  NewsDataSource.swift
//  NewsApp
//
//  Created by Александр on 13/04/2019.
//

import Foundation
import UIKit

class NewsDataSource : GenericDataSource<Article>, UICollectionViewDataSource {
    
    let operationQueue = OperationQueue()
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newsCell", for: indexPath) as! NewsCollectionViewCell
        
        let article = self.data.value[indexPath.row]
        cell.article = article
        
        article.loadImage(){
            [weak self] in
                if (article.loadedImage != nil && indexPath.row < self?.data.value.count ?? 0 ) {
                    collectionView.reloadItems(at: [indexPath])
                }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "loadFooterId", for: indexPath as IndexPath)
            return footerView
        }
        return UICollectionReusableView()
    }
}
