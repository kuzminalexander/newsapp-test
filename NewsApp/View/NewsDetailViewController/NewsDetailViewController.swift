//
//  NewsDetailViewController.swift
//  NewsApp
//
//  Created by Александр on 16/04/2019.
//

import UIKit

class NewsDetailViewController: UIViewController {
    
    var viewModel : NewsDetailViewModel!
    
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var articleAuthorLabel: UILabel!
    @IBOutlet weak var articleDateLabel: UILabel!
    @IBOutlet weak var articleDescriptionTextView: UITextView!
    
    
    convenience init(article : Article){
        self.init()
        self.viewModel = NewsDetailViewModel(article: article)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupContent()
    }
    
    func setupView(){
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.black.cgColor]
        gradientLayer.locations = [0.0, 0.7]
        gradientLayer.frame = view.bounds
        self.articleImageView.layer.insertSublayer(gradientLayer, at: 0)
    }

    func setupContent(){
        self.articleImageView.image = self.viewModel.article.loadedImage
        self.articleTitleLabel.text = self.viewModel.article.title
        self.articleAuthorLabel.text = self.viewModel.article.author
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "dd-MM-yyyy"
        if let publishedDate = self.viewModel.article.publishedDate{
            self.articleDateLabel.text = dateFormatter.string(from: publishedDate)
        } else{
            self.articleDateLabel.text = ""
        }
        self.articleDescriptionTextView.text = self.viewModel.article.body
    }
    
}
