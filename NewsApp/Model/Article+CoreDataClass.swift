//
//  Article+CoreDataClass.swift
//  NewsApp
//
//  Created by Александр on 14/04/2019.
//
//

import Foundation
import CoreData
import UIKit

@objc(Article)
public class Article: NSManagedObject {

    var loadedImage : UIImage?
    var loadingState : LoadState = .free
    
    public func loadImage(completion:@escaping()->()){
        guard loadedImage == nil else{
            return
        }
        if(self.urlToImage?.isEmpty ?? true || self.loadingState == .loading){
            return
        }
        self.loadingState = .loading
        let loadOperation = ImageLoadOperation(url: URL(string: self.value(forKey: "urlToImage") as! String))
        loadOperation.completionBlock = {
            OperationQueue.main.addOperation { [weak self] in
                self?.loadedImage = loadOperation.outputImage
                self?.loadingState = .free
                print("MYLOG: \(ImageLoadOperation.getOperationQueue().operations.count) Loading images stack size")
                completion()
            }
        }
        ImageLoadOperation.getOperationQueue().addOperation(loadOperation)
        print("MYLOG: \(ImageLoadOperation.getOperationQueue().operations.count) Loading images stack size")
    }
    
}
