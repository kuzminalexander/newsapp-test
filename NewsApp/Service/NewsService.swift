//
//  NewsService.swift
//  NewsApp
//
//  Created by Александр on 13/04/2019.
//

import Foundation

protocol NewsServiceProtocol : class {
    func fetchNews(page:Int, pageSize:Int, completion: @escaping ((Result<News, ErrorResult>) -> Void))
}

final class NewsService : RequestHandler, NewsServiceProtocol {
    
    static let shared = NewsService()
    
    var task : URLSessionTask?
    
    func fetchNews(page:Int, pageSize:Int, completion: @escaping ((Result<News, ErrorResult>) -> Void)) {
        let weekAgoDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale.current
        let endpoint = "https://newsapi.org/v2/everything?from=\(formatter.string(from: weekAgoDate!))&q=Apple&sortBy=popularity&apiKey=26c633267ba44d1380c32c19f7446de2&page=\(page)&page_size=\(pageSize)"
        
        self.cancelFetchNews()
        task = RequestService().loadData(urlString: endpoint, completion: self.networkResult(completion: completion))
    }
    
    func cancelFetchNews() {
        
        if let task = task {
            task.cancel()
        }
        task = nil
    }
}
