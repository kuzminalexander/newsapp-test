//
//  OperationUIImage.swift
//  NewsApp
//
//  Created by Александр on 13/04/2019.
//

import Foundation
import UIKit

extension UIImageView{
    
    public func setImageUrl(url:String){
        let loadOperation = ImageLoadOperation(url: URL(string: url))
        loadOperation.completionBlock = {
            OperationQueue.main.addOperation {
                self.image = loadOperation.outputImage
            }
        }
        ImageLoadOperation.getOperationQueue().addOperation(loadOperation)
    }
}
