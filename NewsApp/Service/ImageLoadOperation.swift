//
//  ImageLoadOperation.swift
//  NewsApp
//
//  Created by Александр on 13/04/2019.
//

import Foundation
import UIKit

class ImageLoadOperation: AsyncOperation {
    var url: URL?
    var outputImage: UIImage?
    private static var downloadQueue: OperationQueue?
    
    public static func getOperationQueue () -> OperationQueue{
        if let queue = self.downloadQueue{
            return queue
        }else{
            self.downloadQueue = OperationQueue()
            self.downloadQueue!.name = "load_image_queue"
            self.downloadQueue!.maxConcurrentOperationCount = 5
            return downloadQueue!
        }
    }
    
    init(url: URL?) {
        self.url = url
        super.init()
    }
    
    public func asyncImageLoad(imageURL: URL, completion:@escaping (UIImage?) -> () ) {
        
        let task = URLSession.shared.dataTask(with: imageURL){
            (data, response, error) in
            guard let data = data else {
                completion(UIImage())
                return
            }
            completion(UIImage(data: data))
        }
        task.resume()
    }
    
    override func main() {
        if let imageURL = url {
            asyncImageLoad(imageURL: imageURL) { [unowned self]  image in
                self.outputImage = image
                self.state = .finished
            }
        }
    }
}


class AsyncOperation: Operation {
    
    enum State: String {
        case ready, executing, finished
        
        fileprivate var keyPath: String {
            return "is" + rawValue.capitalized
        }
    }
    
    var state = State.ready {
        willSet {
            willChangeValue(forKey: newValue.keyPath)
            willChangeValue(forKey: state.keyPath)
        }
        didSet {
            didChangeValue(forKey: oldValue.keyPath)
            didChangeValue(forKey: state.keyPath)
        }
    }
}

extension AsyncOperation {
    override var isReady: Bool {
        return super.isReady && state == .ready
    }
    
    override var isExecuting: Bool {
        return state == .executing
    }
    
    override var isFinished: Bool {
        return state == .finished
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override func start() {
        if isCancelled {
            state = .finished
            return
        }
        main()
        state = .executing
    }
    
    override func cancel() {
        state = .finished
    }
    
}
