//
//  File.swift
//  TemplateProject
//
//  Created by Benoit PASQUIER on 13/01/2018.
//

import Foundation

enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case custom(string: String)
}
